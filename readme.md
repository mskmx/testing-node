Proyecto para conocer nivel de experiencia con NodeJs, ExpressJs, mongodb y angularjs (Full-stack)

Requisitos:
1. Crear una rama en este repositorio, con su nombre o nickname, 
2. Generar un formulario con cuatro campos con las siguientes características: (/form)
	a) Un campo deberá ser un select.
	b) Un campo deberá ser un select dependiente de otro (Ejemplo: 1. Select de Estados, 2. Select de Municipios del estado seleccionado en el primero)
	c) Un campo deberá ser numérico y no aceptar otro tipo de caracteres.
	- Propuesta de campos del Form:
		-Nombre, -Edad, -Estado, -Municipio
3. Generar una lista que muestre los datos guardados (/list)
	a) Se deberán desplegar todos sus datos en forma de una lista (grid o card).

Características:
1. Usar Mongodb como base de datos.
2. Hacer el formulario con Angular (Para la carga del 2 select), de preferencia, angular 1, **no es obligatorio**

Deseable:
1. Usar mongoose como ODM [Mongoose](http://mongoosejs.com/)
2. Recibir archivos del formulario.
3. Usar algún framework para el html ( boostrap, materialize, etc)